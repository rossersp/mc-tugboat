#! /bin/bash

loc=${BASH_SOURCE%/*};
if [[ -L ${loc}/tugboat ]]; then
    loc=$(dirname $(readlink -f ${loc}/tugboat));
fi
name=$(basename $(readlink -e $loc))

docker run \
  -itd \
  -p 5901:5901 \
  -p 6901:6901 \
  --name $name \
  --hostname $name \
  --device /dev/dri \
  --cap-add SYS_ADMIN \
  --restart=always \
  $name
