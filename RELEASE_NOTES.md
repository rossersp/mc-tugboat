### 2019-01-30 - 0.1.3

* added java 8 to bigbang
* added maven to pcf
* added pcf

### 2019-01-16 - 0.1.2

* updated jdk target for intellij, added back the accepted file

### 2019-01-11 - 0.1.1

* bug where base-oel-min would flipflop email and fullname

### 2019-01-06 - 0.1.0

* initial release
