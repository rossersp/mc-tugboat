#! /bin/bash

logname='tugboat.log'

# {{{ exit error codes
declare -A exitcodes
exitcodes=(
  ["needs_coreutils"]=1
  ["already_onboard"]=2
);

# }}}
# {{{ tool check
rl='readlink'
date='date'
stat='stat'
if [[ "$(uname)" == "Darwin" ]]; then
  if [[ ! $(which greadlink) ]]; then
    echo "you're on a mac and will need to install coreutils, eg  brew install coreutils"
    exit ${exitcodes["needs_coreutils"]};
  fi
  # use coreutils if on mac
  rl='greadlink'
  date='gdate'
  stat='gstat'
fi
 # }}}
#{{{ location
location()
{
  local loc=${BASH_SOURCE%/*};
  if [[ -L ${loc}/tugboat ]]; then
     loc=$(dirname $($rl -f ${loc}/tugboat));
  fi
  printf '%s' $loc
}
#}}}
#{{{ colorize
FORMATTING=(
    "<b>" $( tput bold )
    "<s>" $( tput smso )
    #"<i>" $( tput sitm )
    "<u>" $( tput smul )
    "<r>" $( tput sgr0 )
    "<R>" $( tput setaf 196 )
    "<G>" $( tput setaf 82 )
    "<B>" $( tput setaf 39 )
    "<Y>" $( tput setaf 226 )
    "<V>" $( tput setaf 129 )
    "<E>" $( tput setaf 248 )
)
NR_FORMATTING="${#FORMATTING[@]}"
for (( i = 0; i < NR_FORMATTING; i += 2 )); do
    SED_DELETE+=" -e s/${FORMATTING[i]}//g"
    SED_REPLACE+=" -e s/${FORMATTING[i]}/${FORMATTING[i + 1]}/g"
done

chalk()
{
  local input=$1
  input=$(sed -e "s/ /\\\x20/g" <<< "$input")
  local text=$( sed $SED_REPLACE <<< "$input<r>" )
  local clear_text=$( sed $SED_DELETE <<< "$input" )
  echo -e $text
  echo $clear_text >> $(location)/$logname
}

error()
{
  chalk "<R>$1"
}

warn() 
{
  chalk "<Y>$1"
}

info()
{
  chalk "$1"
}

debug()
{
  if [[ ! -z $DEBUG ]]; then
    chalk "<V>$1"
  fi
}

rwl()
{
  debug "running with log '$1' in $(pwd)..."
  if [[ ! -z $DEBUG ]]; then
    bash -l -c "$1 2>&1 | tee -a $(location)/$logname";
  else
    bash -l -c "$1 2>&1 > $(location)/$logname";
  fi
}
#}}}
# {{{ environment check

if [[ ! -z $TUGBOAT_ENVIRONMENT ]]; then
  warn "you're already _on_ a tugboat called $TUGBOAT_ENVIRONMENT.  Maybe you want to get off first?"
  exit ${exitcodes["already_onboard"]};
fi

# }}}
# {{{ scuttle
scuttle()
{
  local tugboatName=$1;
  if [[ -z $tugboatName ]]; then
    error "tugboat missing.  Sadface.";
    help;
  fi

  # does this tugboat have child images?
  debug "deriving children for $tugboatName"
  local tugiid=$(docker images "$tugboatName" -q);
  local children=$(for i in $(docker images -q)
  do
    docker history $i | grep -q "$tugiid" && echo $i
  done | sort -u | grep -v "$tugiid");

  info "scuttling children for $tugboatName"
  for child in $children
  do
    childname=$(docker images --format "{{.Repository}} {{.ID}}" | grep $child | sed -E 's/(.+)[[:space:]]+.+/\1/');
    scuttle $childname
  done

  local containers=$(docker ps -a --format "{{.ID}} {{.Names}}" | grep -E "$tugboatName" | sed -E "s/(.+) .+/\1/g");
  if [[ ! -z $containers ]]; then
    info "removing containers matching $tugboatName";
    debug $containers
    # do our best, but we don't _really_ care if kill kills
    docker kill $containers 2>&1 >/dev/null
    docker rm -f $containers;
    sleep 2s;
  fi

  local images=$(docker images -a --format "{{.ID}} {{.Repository}}" | grep -E "$tugboatName" | sed -E "s/(.+) .+/\1/g");
  if [[ ! -z $images ]]; then
    info "removing images matching $tugboatName";
    debug $images
    docker rmi -f $images
  fi

  local danglers=$(docker images -q --filter "dangling=true");
  if [[ ! -z $danglers ]]; then
    info "cleaning up dangling images..."
    docker rmi -f $danglers;
  fi
}
#}}}
#{{{ nuke
nuke()
{
  chalk "Are you <b>sure<r> you want to destroy the docks? (y/n): \c"
  read sure
  if [ "$sure" != 'y' ]; then
    chalk "<G>So you're not sure then...";
    return 1;
  fi
  chalk "      <R><b>LOOK OUT YE LADS"
  info '     _.-^^---....,,--       '
  info ' _--                  --_  '
  info '<                        >)'
  info '|                         | '
  info ' \._                   _./  '
  info '    ```--. . , ; .--\x27\x27\x27       '
  info '          | |   |             '
  info '       .-=||  | |=-.   '
  info '       `-=#$%&%$#=-\x27   '
  info '          | ;  :|     '
  info ' _____.,-#%&$@%#&#~,._____'
  info '    REMEMBER THE HALIFAX'

  tugboats=$(find $(location) -mindepth 1 -maxdepth 1 -type d -not -path '*/\.*' -exec basename {} \;);
  for tugboat in $tugboats
  do
    info "[[[[ NUKE ]]]] scuttling $tugboat ..."
    scuttle $tugboat
  done
}
#}}}
# {{{ build

build()
{
  local name=$1
  local dockerfile="$(location)/$name/Dockerfile"

  # pull the argnames out
  args=($(cat $dockerfile | grep '^ARG[[:space:]]*[[:alnum:]_]*$' | sed -E 's/ARG[[:space:]]+([[:alnum:]_]+)/\1/'))
  buildargs=()

  for arg in "${args[@]}"
  do
    # check the environment for existence
    # if one isn't there, warn and move on
    # note: ${!name} is variable indirection in bash
    if [[ -z ${!arg} ]]; then
      warn "the environment variable $arg is not set."
    else
      debug "$arg is set to ${!arg}";
    fi

    # add the ARG to the buildargs
    quoted="\"${!arg}\"";
    buildargs[${#buildargs[@]}]="--build-arg ${arg}=$quoted"
  done

  buildargs[${#buildargs[@]}]="-t $name:latest ."
  argstring=$(printf '%s ' ${buildargs[@]});
  debug "docker build $argstring";

  rwl "docker build $argstring"
}

run()
{
  local name=$1
  local dockerfile="$(location)/$name/Dockerfile"
  local srcdir=$($rl -f $(location)/..);

  # pull the ports out
  ports=($(cat $dockerfile | grep '^EXPOSE[[:space:]]' | sed -E 's/EXPOSE[[:space:]]+([[:alnum:]\/]+)/\1/'))
  runargs=()

  for port in "${ports[@]}"
  do
    numericPort=$(printf '%s' $port | sed -E 's/([[:alnum:]]+)\/.+/\1/')
    debug "adding $numericPort:$port"
    runargs[${#runargs[@]}]="-p $numericPort:$port"
  done

  debug "$srcdir"
  runargs[${#runargs[@]}]="--name $name"
  runargs[${#runargs[@]}]="-v $srcdir:/src"
  runargs[${#runargs[@]}]="--hostname $name"
  if [[ $(hostname) != 'penguin' ]]; then
	# crostini does not like it when you try to run a priveleged container
  	runargs[${#runargs[@]}]="--privileged"
  fi
  # there's a minor bug with using run vs exec where on a first-time run,
  # the cols and lines of the bash session started don't honor the host's
  # dimensions
  # see moby #33794 for more info
  runargs[${#runargs[@]}]="-e COLUMNS=\"$(tput cols)\" -e LINES=\"$(tput lines)\""
  runargs[${#runargs[@]}]="-it $name /bin/bash"

  argstring=$(printf '%s ' ${runargs[@]});
  debug "docker run $argstring";

  rwl "docker run $argstring"

}

# }}}
#{{{ resolve
# check to see if the `FROM` directive is a local tugboat image
resolve()
{
  local toCheck=$1;
  local checkDir=$(location)/$toCheck;
  validate $checkDir

  # check $1 to see if it's a local guy,
  if [[ $? -eq 0 ]]; then
    # if so recurse
    debug "-- $toCheck found, recursing"
    local result=$(cat "${checkDir}/Dockerfile" | grep FROM | sed -E 's/FROM (.+)\:(.+)/\1/');
    resolve $result;
   
    debug "-- checking if $toCheck is built"
    # otherwise, is the image built?
    docker images $toCheck --format "{{.Repository}}" | grep $toCheck > /dev/null;
    local built=$?;

    if [[ $built -eq 0 ]]; then
      # ensure the build is up to date
      local imgdate=$(docker inspect $toCheck --format "{{.Created}}" | xargs -n1 date +%s -d);
      local filedate=$(find $checkDir -type f -exec stat -c %Y {} \; 2>/dev/null | sort -r | head -n 1);
      debug "checking build dates - image $imgdate, file $filedate"
      if [[ $imgdate -le $filedate ]]; then
        local offender=$(find $checkDir -type f -exec stat -c "%Y %n" {} \; 2>/dev/null | sort -r | head -n 1);
        # if not, scuttle the image
        debug "file \"${offender}\" is newer.  scuttling $toCheck."
        scuttle $toCheck
        built=1
      fi
    fi

    # build if not
    if [ $built -ne 0 ]; then
      warn "\t docker image $toCheck is a dependency that needs to be built first.  Building."
      #read -n 1 -s -r -p "Press any key to continue"
      pushd $checkDir > /dev/null;
      #rwl "docker build --build-arg email=\"$EMAIL\" --build-arg full_name=\"$FULL_NAME\"-t $toCheck:latest .";
      build $toCheck;
      popd > /dev/null;
    fi
  fi
}
#}}}
#{{{ help
help() 
{
  cat << HALPPLZ

Tugboat: Create small, destroyable development environments!

Usage: tugboat [subcommand] [subcommand arguments]

Subcommands:
  help              prints this help
  use [name]        enter the named environment
  scuttle [name]    destroys the named environment
  nuke              destroys all of your environments.  All. of. them.
HALPPLZ
  exit 0;
}
#}}}
#{{{ validate
validate()
{
  local tugboatFullPath=$1
  debug "validating $tugboatFullPath"
  if [[ -d $tugboatFullPath ]]; then
    debug "$tugboatFullPath is a valid directory."
    if [[ ! -f $tugboatFullPath/Dockerfile ]]; then
      error "no dockerfile for $tugboatFullPath.  this is bad!"
      return 5;
    fi
  elif [[ -f $tugboatFullPath ]]; then
    error "$tugboatFullPath is a file.  this is not supported... yet."
    return 99;
  else
    info "$tugboatFullPath could not be found locally"
    return 3;
  fi
  return 0;
}
#}}}
#{{{ use
use()
{
  local tugboatName=$1;
  debug "use $tugboatName"
  if [[ -z $tugboatName ]]; then
    error "tugboat $tugboatName missing.  Sadface.";
    help;
  fi
  
  local srcdir=$($rl -f $(location)/..);
  debug "use location: $(location)"

  dockerDirectory=$(location)/$tugboatName;
  validate $dockerDirectory
  if [[ $? -ne 0 ]]; then
    exit $?;
  fi

  # make sure all of the upstream images that are local are built and ready to go
  resolve $tugboatName
  debug "finished resolving the upstream... let's do this!"

  if [[ -d ~/.ssh ]]; then
    cp -ru ~/.ssh $dockerDirectory;
  fi

  # is the dockerfile running as a container?
  docker ps -f "name=$tugboatName" --format "{{.Names}}" | grep -E "^${tugboatName}$" > /dev/null;
  running=$?;
  if [ $running -eq 0 ]; then
    info "container '$tugboatName' is running, attaching new bash instance.";
    #yes, attach to the docker instance
    docker exec -it $tugboatName "/bin/bash";
    exit 0;
  fi

  # does the container exist but is stopped?
  docker ps -a -f "name=$tugboatName" --format "{{.Names}}" | grep -E "^${tugboatName}$" > /dev/null;
  stopped=$?;
  if [ $stopped -eq 0 ]; then
    info "container '$tugboatName' is stopped, starting and attaching new bash instance.";
    #yes, start and attach to the docker instance
    docker start -i $tugboatName;
    exit 0;
  fi

  # is the image built?
  docker images $tugboatName --format "{{.Repository}}" | grep $tugboatName > /dev/null;
  built=$?;
  if [ $built -eq 0 ]; then
    info "docker image '$tugboatName', is built but a container is not running.  Running.";
    # yes, run the image with the same name
    if [[ -f $dockerDirectory/run.sh ]]; then
      debug "run.sh found, assuming y'all know what you're doing in ${dockerDirectory}"
      bash -l $dockerDirectory/run.sh
      exit 0;
    else
      run $tugboatName
      exit 0;
    fi
  fi

  # ooootherwise, build the image and run
  info "docker image '$tugboatName' exists but is not built. Building and running.";
  pushd $dockerDirectory > /dev/null;
  #rwl "docker build --build-arg email=\"$EMAIL\" --build-arg full_name=\"$FULL_NAME\"-t $toCheck:latest .";
  #rwl "docker build -t $tugboatName:latest .";
  build $tugboatName;
  if [[ -f $dockerDirectory/run.sh ]]; then
    debug "run.sh found, assuming y'all know what you're doing in ${dockerDirectory}"
    bash -l $dockerDirectory/run.sh
    exit 0;
  else
    run $tugboatName
    popd > /dev/null;
  fi
}
#}}}
#{{{ tree
mktree()
{
  local toCheck=$1
  local chain=$2
  local tmpdir=$3

  if [[ -z $tmpdir ]]; then
    tmpdir=$(mktemp -d)
  fi

  local toCheck=$1;
  local checkDir=$(location)/$toCheck;
  validate $checkDir

  # check $1 to see if it's a local guy,
  if [[ $? -eq 0 ]]; then
    # if so recurse
    debug "[mktree] $toCheck found, recursing"
    local result=$(cat "${checkDir}/Dockerfile" | grep FROM | sed -E 's/FROM (.+)\:(.+)/\1/');
    chain="$toCheck/$chain"
    debug "[mktree] chain: $chain"
    mktree $result $chain $tmpdir
  else
    debug "here, we would make $tmpdir/$toCheck/$chain"
    mkdir -p $tmpdir/$toCheck/$chain
  fi
}

printtree()
{
  local dir=$1
  local tabs=${2:-0}
  local elbow=$3
  local tmpdir=$4
  local boats=($(find $dir -maxdepth 1 -mindepth 1 -type d -not -path '*/\.*'))

  if [[ $dir == $tmpdir ]]; then
    echo "$elbow tugboat"
    tabs=-1
  else
    printf '%.0s    ' $(seq 0 $tabs)
    printf '%s\n' "${elbow} $(basename $dir)"
  fi

  if [[ ${#boats[@]} -gt 0 ]]; then
    local last=${boats[-1]}

    for i in "${boats[@]}"
    do
      if [[ $last == $i ]]; then
        elbow='└── ';
      else
        elbow="├── "
      fi
      printtree $i $(($tabs+1)) $elbow $tmpdir
    done
  fi
}

tree()
{
  local tmpdir=$(mktemp -d)
  boats=$(find $(location) -maxdepth 1 -mindepth 1 -type d -not -path '*/\.*' -exec basename {} \;)

  for i in $boats
  do
    debug "checking tree for $i"
    mktree $i "" $tmpdir
  done

  /usr/bin/tree $tmpdir

  printtree $tmpdir 0 '  └' $tmpdir
}

#}}}


#########################
# execution tiiiiiiiime #
#########################

if [ "$#" -eq 0 ]; then
  help
fi

if [ ! -d ~/.ssh ]; then
  warn "warning: looks like you haven't set up ssh yet.  Why don't you go do that first?"
fi

# $1 will be the subcommand
subcommand=$1;
shift;

# call the subcommand with the remaining arguments
$subcommand "$@"
