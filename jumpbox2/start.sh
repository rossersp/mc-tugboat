#!/bin/bash

vncserver -kill :1 || rm -rfv /tmp/.X*-lock /tmp/.X11-unix || echo "remove old vnc locks to be a reattachable container"
startxfce4 &
vncserver :1

VNC_IP=$(ip addr show eth0 | grep -Po 'inet \K[\d.]+')
/novnc/utils/launch.sh \
    --vnc $VNC_IP:5901 \
    --listen 6901 &

#/usr/bin/google-chrome-stable --make-default-browser --no-sandbox \
tail -f /dev/null
