#! /bin/bash
gitrepo=$1

# extract the directory name from the gitrepo uri
dirname=$(echo $gitrepo | sed -E 's/.+\/(.+)\.git/\1/')

# start dockerd
dockerd & 2>&1 > /dev/null

# run patchports
./patchports-sed.sh /$dirname

# converge
pushd /$dirname
kitchen converge
popd

# await dockerd
# wait
