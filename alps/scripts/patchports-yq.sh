#!/bin/bash
directory=$1

update=$(cat $directory/.kitchen.yml | yq -r '.suites[] | select(.name=="default").driver_config.forward' | sed -E '/[0-9+]/ {/:/! s/([0-9]+)/"\1:\1"/g}')

suiteupdate=$(cat $directory/.kitchen.yml | yq ".suites[] | select(.name==\"default\").driver_config.forward |= $update")

cat $directory/.kitchen.yml | yq -y ".suites |= $suiteupdate" > $directory/updated.yml

cp $directory/.kitchen.yml $directory/kitchen.yml.original
mv $directory/updated.yml $directory/.kitchen.yml

