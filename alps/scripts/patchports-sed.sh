#!/bin/bash
directory=$1

# back up the OG kitchen yml
cp $directory/.kitchen.yml $directory/kitchen.yml.original

# figure out where the forward block is
start=$(awk '/forward:/{print NR; exit}' $directory/.kitchen.yml)
# get total lines
total=$(wc -l $directory/.kitchen.yml | sed -E 's/([[:alnum:]]+)[[:space:]].+/\1/')
# determine indenation level
indent=$(sed -n ${start}p $directory/.kitchen.yml | sed -E 's/([[:space:]]+).+/\1/')

echo "start is ${start}, total ${total}, indent level is ${indent}"

# for each line after the start,
for (( line=$start; line<=$total; line++ ))
do
  echo "line ${line}"
  # get the line indent
  lineIndent=$(sed -n ${line}p $directory/.kitchen.yml | sed -E 's/([[:space:]]+).+/\1/')
  # if the current line indent is outside of the forward stanza, break
  if [[ ${#indent} -gt ${#lineIndent} ]]; then
    break
  fi
  # otherwise, patch the port to forward to a hard-set port, eg 2222:2222
  # nb, this could probably be done with line addressing in some clever way, but
  # sed balks at the line number between the addressing and substitution directive
  # example:
  #sed -i -E "/:/!${line}s/([0-9]+)/\1:\1/}" $directory/.kitchen.yml 

  linetext=$(sed -n ${line}p $directory/.kitchen.yml)
  if sed -n ${line}p $directory/.kitchen.yml | grep -v ':'; then
    sed -i -E "${line}s/([0-9]+)/\1:\1/" $directory/.kitchen.yml
  fi
done
