FROM base-alpine:latest
EXPOSE 8086
USER ross

ARG ARM_CLIENT_ID
ARG ARM_CLIENT_SECRET
ARG AD_CLIENT_ID
ARG AD_CLIENT_SECRET
ARG ARM_SUBSCRIPTION_ID
ARG ARM_TENANT_ID
ARG ARM_ENROLLMENT_ACCOUNT_ID

ENV ARM_CLIENT_ID=${ARM_CLIENT_ID}
ENV ARM_CLIENT_SECRET=${ARM_CLIENT_SECRET}
ENV AD_CLIENT_ID=${AD_CLIENT_ID}
ENV AD_CLIENT_SECRET=${AD_CLIENT_SECRET}
ENV ARM_SUBSCRIPTION_ID=${ARM_SUBSCRIPTION_ID}
ENV ARM_INFRA_SUBSCRIPTION_ID=${ARM_SUBSCRIPTION_ID}
ENV ARM_LOGGING_SUBSCRIPTION_ID=${ARM_SUBSCRIPTION_ID}
ENV ARM_TENANT_ID=${ARM_TENANT_ID}
ENV ARM_ENROLLMENT_ACCOUNT_ID=${ARM_ENROLLMENT_ACCOUNT_ID}
ENV SRC_DIR=/src/clod/tf

ENV TF_VERSION=0.14.11

USER root
RUN mkdir -p /apps_data_01/security/keystores/cacerts-prod && \
    curl -o /apps_data_01/security/keystores/cacerts-prod/cacerts.pem -kslO https://globalrepository.mclocal.int/stash/projects/WEBADMINCHEF/repos/mc_chef_client/raw/files/default/cacert.pem?at=refs%2Fheads%2Fmaster && \
    curl -o /terraform_${TF_VERSION}_linux_amd64.zip -kslO https://releases.hashicorp.com/terraform/${TF_VERSION}/terraform_${TF_VERSION}_linux_amd64.zip && \
    unzip -o /terraform_${TF_VERSION}_linux_amd64.zip terraform && \
    ln -s /terraform /usr/local/bin/tf && \
    ln -s /terraform /usr/local/bin/terraform

USER ross
RUN pip3 install reno && \
    pip3 install azure.cli

RUN echo "export PATH=\"${PATH}:/ross/.local/bin\"" >> ~/.bashrc && \
    echo "alias 'tfc'='cd /src/clod/tf/tf-azure-consumables'" >> ~/.bashrc && \
    echo "alias 'tfr'='cd /src/clod/tf/tf-azure-resources'" >> ~/.bashrc && \
    echo "alias 'tfm'='cd /src/clod/tf/mercator'" >> ~/.bashrc && \
    echo "source /src/test_onboard_env.sh" >> ~/.bashrc && \
    echo "echo 'logging into sbox...'" >> ~/.bashrc && \
    echo "azlogin sbox" >> ~/.bashrc && \
    echo "sudo chown -R ross:ross /src" >> ~/.bashrc

ENV TUGBOAT_ENVIRONMENT="atlas-tf14-dev"
