#!/bin/bash
git=/usr/bin/git
make=/usr/bin/make

repos=(
  'log'
  'api-helper'
  'helper'
  'monkeypatch'
  'util'
  'renderer'
  'execute'
  'deployspec-compiler'
)

for repo in "${repos[@]}"
do
  pushd /src/clod/atlas/azure-cloudcore-$repo
  currentBranch=$($git rev-parse --abbrev-ref HEAD)
  $git checkout master
  $git pull upstream master
  $git push origin master
  $git checkout $currentBranch
  $make setup
  $make install
  popd
done
