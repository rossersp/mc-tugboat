FROM ubuntu:18.04
SHELL ["/bin/bash", "-c"] 

# install all of the relevant parts
RUN apt-get update && \
    apt-get install -y \
    curl \
    gnupg \
    && \
    
    # add the gpg key for rvm
    gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3 7D2BAF1CF37B13E2069D6956105BD0E739499BDB && \
    
    # add nodejs
    curl -sL https://deb.nodesource.com/setup_8.x | bash - && \

    apt-get install -y \
        build-essential \
        dtrx \
        git \
        nmap \
        nodejs \
        openssh-client \
        python-dev \
        python3-dev \
        python3-pip \
        tmux \
        vim \
        wget \
        build-essential \
        cmake \
        && \
    
    # install thefuck
    /usr/bin/pip3 install thefuck && \
    
    # pull down gitbash
    git clone https://github.com/magicmonty/bash-git-prompt.git ~/.bash-git-prompt && \

    # make vim pathogen directory, tmux configuration directory 
    mkdir -p ~/.vim && mkdir -p ~/.tmux && \

    # pull down dotfiles
    
    # pull down the tern config
    wget -O ~/.tern-config https://raw.githubusercontent.com/rosshinkley/dotfiles/master/.tern-config && \

    # pull down vundle
    git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim && \

    # prop up formatters
    npm install -g \
        js-beautify \
        typescript \
        typescript-formatter

# copy the ssh key local -> image
COPY .ssh/ /root/.ssh/

# for now, copy the local vimrc out
COPY vimrc /root/.vimrc

# for now, copy the local tmux conf out too
COPY tmux.conf /root/.tmux.conf

# and for teh hat trick, the bashrc
COPY bashrc /root/.bashrc

# add the sdkman config
COPY sdkman-config /root/sdkman-config


# instruct vundle to install the plugins
# hack: echo twice gets past vim complaining about zenburn
# hack: in the future, this could probably be accomplished with env variables when the set color func is called
# hack: this is from https://github.com/VundleVim/undle.vim/issues/511
RUN echo | echo | vim +PluginInstall +qall &>/dev/null

# set up the environment
ENV TUGBOAT_ENVIRONMENT='base-deb'

# install mono
#RUN echo -e "" && \
#    apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 3FA7E0328081BFF6A14DA29AA6A19B38D3D831EF && \
#    apt-get install -y apt-transport-https && \
#    echo "deb https://download.mono-project.com/repo/ubuntu stable-xenial main" | tee /etc/apt/sources.list.d/mono-official-stable.list && \
#    apt-get update && \
#    apt-get install -y mono-devel

#ENV PATH="${PATH}:/usr/local/go/bin"

#RUN echo -e "" && \
    # install ycm
#    ~/.vim/bundle/YouCompleteMe/install.py \
#        --cs-completer \
#        --clang-completer \
#        --go-completer \
#        --js-completer \
#        && \
#    echo "ycm installed"

# add the ssh key
RUN eval "$(ssh-agent -s)" && \
    ssh-add ~/.ssh/id_rsa && \
        
    # set up git identity
    git config --global user.email "rosshinkley@gmail.com" && \ 
    git config --global user.name "ross hinkley" && \
    echo "finished main prop up"


RUN echo -e "" && \
    # install ruby
    curl -sSL https://get.rvm.io | bash -s stable && \
    echo -e "\n\n === about to list /etc/profile.d" && \
    ls /etc/profile.d && \
    echo -e " === \n\n"&& \
    source /etc/profile.d/rvm.sh && \
    rvm install ruby-2.4.1 && \

    # install bundler
    gem install bundler && \

    echo "done"


