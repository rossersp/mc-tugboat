#! /bin/bash
pushd /home/ross/jumpbox/renew
(http-server -p 80 .) &
serverpid=$!
echo $serverpid;
certbot renew --dry-run

kill -9 $serverpid;
popd;
