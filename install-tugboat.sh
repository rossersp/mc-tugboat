#!/bin/bash

# check for docker being installed
which docker > /dev/null;
if [[ $? -ne 0 ]]; then
  echo "you'll want to install docker first.";
  exit 1;
fi

# gotta be relatively recent
docker --version | grep -E "Docker version [0-9]{2}\.[0-9]{2}" > /dev/null;
if [[ $? -ne 0 ]]; then
  echo "you _probably_ want a more modern version of docker."
  exit 2;
fi

# what about git?
which git > /dev/null;
if [[ $? -ne 0 ]]; then
  echo "you need git to use tugboat.";
  exit 3;
fi

# how about alternatives?
which alternatives > /dev/null;
if [[ $? -ne 0 ]]; then
  echo "tugboat needs alternatives to add a super-awesome shortcut."
  exit 4;
fi

# derive the user calling sudo
user=${SUDO_USER:-$(who|awk '{print $1}')};
# get the user home
userhome=$(awk -v FS=':' -v user="$user" '($1==user) {print $6}'  '/etc/passwd');

mkdir -p ${userhome}/src

# clone the repo down
git clone https://globalrepository.mclocal.int/stash/scm/~e082491/mc-tugboat.git ${userhome}/src/tugboat

chmod -R +rw ${userhome}/src/tugboat;
chown -R $user:$user ${userhome}/src/tugboat;

# add an alternative for tugboat
alternatives --install /usr/bin/tugboat tugboat $userhome/src/tugboat/tugboat.sh 1000
alternatives --set tugboat $userhome/src/tugboat/tugboat.sh
