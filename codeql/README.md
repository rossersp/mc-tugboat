# notes

1. once the container is up and `bash` is attached:
    1. `cd` to a golang repo ([this](https://fusion.mastercard.int/stash/users/e082491/repos/mercator/browse) is the example i used)
    1. `codeql database create [name] --language=go`
        1. note i had to also add `--command='make build'` to get the detection to work right
        1. this will pull together datapoints + finalize the database
            1. you can do the creation + finalizing yourself if you need to trace specific build commands
    1. `codeql database analyze [name] --format=csv --output=./output.csv`
1. by default, the go queries run the default query suite, defined in the [`qlpack`](https://github.com/github/codeql/blob/main/go/ql/src/qlpack.yml)
1. suites can be run by referencing the path
    1. `codeql database analyze [name] --format=csv --output=./output.csv /path/to/installed/pack/location/go-queries/0.4.6/codeql-suites/go-developer-happiness.qls`
1. you can forcibly re-evaluate the queries by adding `--rerun`
1. adding a new pack
    1. `codeql pack init [name]/[packname]`
    1. for workspace-dependent queries, a `codeql-workspace.yml` will need to be added (see [here](./codeql-workspace.yml))
    1. add the dependencies to your `qlpack.yml`
    1. run `codeql pack install`
        1. conversely, you can add `codeql pack add [pack]`, but i couldn't get it working
    1. run the query - `codeql database analyize [name] --format=csv --output=./output.csv ./ql/len.ql`
        1. it's _vital_ that the metadata is there in the query file, specifically `kind`


## references

* [`codeql` cli](https://docs.github.com/en/code-security/codeql-cli/using-the-codeql-cli/about-the-codeql-cli)
* [analyzing the database](https://docs.github.com/en/code-security/codeql-cli/using-the-codeql-cli/analyzing-databases-with-the-codeql-cli)
* [codeql packs](https://github.com/orgs/codeql/packages)
