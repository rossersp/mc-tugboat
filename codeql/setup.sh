#!/bin/bash

echo "setting up codeql" \
&& mkdir -p /src \
&& wget https://github.com/github/codeql-cli-binaries/releases/download/v2.12.6/codeql.zip \
&& unzip -u codeql.zip -d /src \
&& if [[ ! -d /src/codeql/codeql-home ]]; then gh repo clone github/codeql /src/codeql/codeql-home; fi \
&& if [[ ! -d /src/codeql/linguist ]]; then gh repo clone github/linguist /src/codeql/linguist; fi \
&& codeql pack download codeql/go-queries \
&& echo "done setting up codeql"

echo "scan time!"
pushd /src/codeql/linguist
for language in $(github-linguist --json | jq -r '. | keys[]'); do
  codeql database create $language --language=$language
  codeql database analyze $language --format=csv --output=./$language.csv
done
popd
