#!/bin/bash

inotifywait \
  -r \
  -m \
  -e modify \
  -e moved_to \
  -e moved_from \
  -e move \
  -e create \
  -e delete \
  /src/notes | 
  while read -r filename event; do
    if echo $filename | grep -v '.git' 2>&1 >/dev/null; then
      pushd /src/notes > /dev/null
        git add --all 2>&1 1>/dev/null
        git commit --quiet -am "$event on $filename" 2>&1 >/dev/null
        git push --quiet
      popd > /dev/null
    fi
  done
