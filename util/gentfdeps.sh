#!/bin/bash

tfFile=$1

resources=($(cat $tfFile | grep ^resource | grep -v dependency | sed -E 's/resource "(.+?)" "(.+?)".+$/\1.\2.id,/'))
modules=($(cat $tfFile | grep ^module | sed -E 's/module "(.+?)".+/module.\1.depended_on,/'))

for r in ${resources[@]}; do
  echo $r
done

for r in ${modules[@]}; do
  echo $r
done
