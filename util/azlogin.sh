#! /bin/bash

tier=$1

if [[ $tier == "prod" ]]; then
  echo "prod"
  az login -u $PROD_ARM_CLIENT_ID -p $PROD_ARM_CLIENT_SECRET -t $ARM_TENANT_ID --service-principal
  export ARM_CLIENT_ID=$PROD_ARM_CLIENT_ID
  export ARM_CLIENT_SECRET=$PROD_ARM_CLIENT_SECRET
  export ARM_SUBSCRIPTION_ID=$PROD_ARM_SUBSCRIPTION_ID
  export ARM_ENROLLMENT_ACCOUNT_ID=$PROD_ARM_ENROLLMENT_ACCOUNT_ID
elif [[ $tier == "nonp" ]]; then
  echo "nonp"
  az login -u $NONP_ARM_CLIENT_ID -p $NONP_ARM_CLIENT_SECRET -t $ARM_TENANT_ID --service-principal
  export ARM_CLIENT_ID=$NONP_ARM_CLIENT_ID
  export ARM_CLIENT_SECRET=$NONP_ARM_CLIENT_SECRET
  export ARM_SUBSCRIPTION_ID=$NONP_ARM_SUBSCRIPTION_ID
  export ARM_ENROLLMENT_ACCOUNT_ID=$NONP_ARM_ENROLLMENT_ACCOUNT_ID
elif [[ $tier == "work" ]]; then
  echo "work"
  echo $WORK_ARM_CLIENT_ID
  echo $WORK_ARM_CLIENT_SECRET
  echo $ARM_TENANT_ID
  az login -u $WORK_ARM_CLIENT_ID -p $WORK_ARM_CLIENT_SECRET -t $ARM_TENANT_ID --service-principal
  export ARM_CLIENT_ID=$WORK_ARM_CLIENT_ID
  export ARM_CLIENT_SECRET=$WORK_ARM_CLIENT_SECRET
  export ARM_SUBSCRIPTION_ID=$WORK_ARM_SUBSCRIPTION_ID
  export ARM_ENROLLMENT_ACCOUNT_ID=$WORK_ARM_ENROLLMENT_ACCOUNT_ID
elif [[ $tier == "idev" ]]; then
  echo "idev"
  az login -u $IDEV_ARM_CLIENT_ID -p $IDEV_ARM_CLIENT_SECRET -t $ARM_TENANT_ID --service-principal
  export ARM_CLIENT_ID=$IDEV_ARM_CLIENT_ID
  export ARM_CLIENT_SECRET=$IDEV_ARM_CLIENT_SECRET
  export ARM_SUBSCRIPTION_ID=$IDEV_ARM_SUBSCRIPTION_ID
  export ARM_ENROLLMENT_ACCOUNT_ID=$IDEV_ARM_ENROLLMENT_ACCOUNT_ID
elif [[ $tier == "sbox" ]]; then
  echo "sbox"
  cmd="az login -u $SBOX_ARM_CLIENT_ID -p $SBOX_ARM_CLIENT_SECRET -t $ARM_TENANT_ID --service-principal"
  echo $cmd
  $cmd
  export ARM_CLIENT_ID=$SBOX_ARM_CLIENT_ID
  export ARM_CLIENT_SECRET=$SBOX_ARM_CLIENT_SECRET
  export ARM_SUBSCRIPTION_ID=$SBOX_ARM_SUBSCRIPTION_ID
  export ARM_ENROLLMENT_ACCOUNT_ID=$SBOX_ARM_ENROLLMENT_ACCOUNT_ID
else 
  echo "tier $tier not found, try again."
  exit 1
fi
