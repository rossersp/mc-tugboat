#!/bin/bash
function cdrf() {
  echo "going to $(git rev-parse --show-toplevel)"
  cd $(git rev-parse --show-toplevel)
}

export alias cdrf='cdrf'
