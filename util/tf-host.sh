#!/bin/bash

#loc=${BASH_SOURCE%/*}
loc=$(pwd)

if [[ -z $SRC_DIR ]]; then
  echo "set SRC_DIR to your source directory"
  exit 1
fi

pushd $SRC_DIR/tf-azure-consumables
rm -rf ./build
./scripts/build.sh
popd

cp $SRC_DIR/tf-azure-consumables/build/*.tar.gz $loc/tf-azure-consumables.tar.gz

pushd $SRC_DIR/terraform-provider-mcazurerm
rm -f terraform-provider-mcazurerm
go build -o terraform-provider-mcazurerm
popd

pushd $SRC_DIR/terraform-provider-mcecms
rm -f terraform-provider-ecms
go build -o terraform-provider-mcecms
popd


cp $SRC_DIR/terraform-provider-mcazurerm/terraform-provider-mcazurerm $loc
cp $SRC_DIR/terraform-provider-mcecms/terraform-provider-mcecms $loc
ls -al
http-server -p 8081 .
