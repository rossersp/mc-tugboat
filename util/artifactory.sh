#! /bin/bash

# get the version listing from artifactory
versions=($(curl -sSLk -X GET https://artifacts.mastercard.int/artifactory/api/storage/python-internal-unstable/mc-bigbang | \
  grep 'ci_engine' | \
  sed -E 's/[[:space:]]*"uri"[[:space:]]*:[[:space:]]*"\/ci_engine-([0-9]+\.[0-9]+\.[0-9]+).+/\1/g' | \
  sort -r))

currentVersion=$(cat VERSION)
echo $currentVersion

if echo $versions | grep $currentVersion; then
  artifactoryLatest=$(echo $versions | head -n 1)
  majorminor=$(echo $artifactoryLatest | sed -E 's/([0-9]+\.[0-9]+).+/\1/')
  patch=$(echo $artifactoryLatest | sed -E 's/[0-9]+\.[0-9]+\.([0-9]+)/\1/')
  patch=$((patch+1))
  currentVersion="${majorminor}.${patch}"
fi

echo $currentVersion > VERSION
