#!/bin/bash
if [[ -z $1 ]]; then
  echo "no prefix defined."
  exit 1
fi

echo "prefix is $1"
rgs=()

subscription_id=$(az account show | jq -r '.id')

time_cutoff=$(date --date "3 days ago" +'%s')
while read -r rg created_time fixture; do
  created_time_s=$(date --date $created_time +'%s')
  if [[ $time_cutoff -lt $created_time_s ]] && [[ $FORCE != "true" ]]; then
    echo "$rg is too new with created time $created_time, skipping"
    continue
  fi

  if [[ $fixture == "true" ]]; then 
    echo "$rg is a fixture, skipping"
    continue
  fi

  rgs+=($rg)
  echo "removing $rg"
  az group delete -n $rg -y --no-wait
done < <(az rest -u  "https://management.azure.com/subscriptions/$subscription_id/resourcegroups?api-version=2021-04-01&%24expand=createdTime" | jq -r '.value[] | select(.name | test("'$1'")) | "\(.name) \(.createdTime) \(.tags.fixture)"')

while :; do
  echo "${#rgs[@]} remain to be deleted."
  revised_rgs=()
  for rg in "${rgs[@]}"; do
   if ! az group show -g $rg 2>&1 >/dev/null; then
     echo "$rg deleted."
     continue
   fi
   revised_rgs+=($rg)
  done
  if [[ "${#revised_rgs[@]}" -eq 0 ]]; then
    break
  fi
  rgs=("${revised_rgs[@]}")
  sleep 30s
done
