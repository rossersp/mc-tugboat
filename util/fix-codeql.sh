#!/bin/bash

cp /usr/lib/jvm/java-17-openjdk/lib/libjimage.so /src/bse/codeql/tools/linux64/java/lib/libjimage.so
cp /usr/lib/jvm/java-17-openjdk/lib/server/libjvm.so /src/bse/codeql/tools/linux64/java/lib/server/libjvm.so
cp /usr/lib/jvm/java-17-openjdk/lib/server/libjvm.so /src/bse/codeql/tools/linux64/java/libjvm.so
cp /usr/lib/jvm/java-17-openjdk/lib/libjava.so /src/bse/codeql/tools/linux64/java/lib/libjava.so
