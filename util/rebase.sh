#!/bin/bash
defaultFrom=$(git rev-parse --abbrev-ref HEAD)
defaultTo="master"
from=${1:-$defaultFrom}
to=${2:-$defaultTo}

git rebase -i HEAD~$(git rev-list ${from}...${to} --count)
