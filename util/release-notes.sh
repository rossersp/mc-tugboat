#!/bin/bash

input=$1
since=$2

project=$(echo $input | sed -E 's/(.+)\/(.+)/\1/')
repo=$(echo $input | sed -E 's/(.+)\/(.+)/\2/')

if [[ -z $since ]]; then
  since=$(date --date="2 weeks ago" +%s)
fi

curl -f -H \
  "Authorization: Bearer $BITBUCKET_BEARER" \
  "https://globalrepository.mastercard.int/stash/rest/api/1.0/projects/$project/repos/$repo/pull-requests?state=MERGED&withAttributes=false&withProperties=false&limit=1000" \
  | jq ".values[]| select (.closedDate/1000 > $since) | {title, date: (.closedDate/1000 | todateiso8601)}"
