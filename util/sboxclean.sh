#!/bin/bash
source azlogin sbox
az account set -s $ARM_SUBSCRIPTION_ID

:<<'EOF'
fixture_types=(
  "Microsoft.Compute/virtualMachines"
  "Microsoft.Compute/virtualMachineScaleSets"
  "Microsoft.Network/loadBalancers"
  "Microsoft.Network/publicIPAddresses"
  "Microsoft.Compute/disks"
  "Microsoft.Network/networkInterfaces"
  "Microsoft.Cache/Redis"
)

for fixture_type in "${fixture_types[@]}"; do
  echo "cleaning up $fixture_type"
  az resource delete --ids $(az resource list -g transient-apptfactest-svctfactest-sbox-envtfac-eastus --resource-type "$fixture_type" | jq -r '.[] | .id')
done
EOF

regexes=(
  '.*import.*'
  'transient-apptfactest-\\w{6}-sbox-\\w{6,}-eastus'
  'transient-apptfactest-\\w{6}-\\w+-sbox-\\w{6,}-eastus'
  'transient-atlas-mercator.*'
  'rg-cloudcore-network-dns-sbox\\w{6,7}'
  'keyvaults-apptfactest-\\w{6}-sbox-\\w{6,}-eastus'
  'keyvaults-apptfactest-\\w{6}-\\w+-sbox-\\w{6,}-eastus'
  'keyvaults-atlas-mercator.*'
  'persistent-apptfactest-\\w{6}-sbox-\\w{6,}-eastus'
  'persistent-apptfactest-\\w{6}-\\w+-sbox-\\w{6,}-eastus'
  'persistent-atlas-mercator.*'
  'network-ta-.*'
  'keyvaults-ta-.*'
  'network-BuilderTools.*'
  'network-ta-.*'
  'rg-sbox\\w{9}-.*'
  'tf-azure-resources-.*'
  'transient-ta-.*'
  'persistent-ta-.*'
  'hsm-ta.*'
  'rg-hsm-infra-test.*'
  'network-imagebake-sboxsrc.*'
  'persistent-imagebake.*'
  'transient-imagebake.*'
  'keyvaults-imagebake.*'
  'rg-eh-*'
  'rg-ta-.*'
  'acctestRG.*'
  'rg-sbox\\w{6}-.*'
  'rg-srcsbox\\w{6}-.*'
  'alert-actions-.*-sbox\\w{6}.*'
  'alert-actions-\\w{6}-sbox-eastus'
  'Alert-actions-.*-sbox\\w{6}.*'
  'imagerg'
  'aks-\\w{7}-sbox-eastus'
  'network-.*-sbox\\w{6}.*'
  'mercator-test-.*'
  'rg-sboxdest.*'
  'keyvaults-eh-.*'
  'persistent-eh-.*'
  'transient-eh-.*'
  'network-\\w{6}-sbox-eastus'
  'rg-sbox-eastus-\\w{6}-tfstate'
)

guts=$(printf '|%s' "${regexes[@]}")
guts=${guts:1}

regex="($guts)"
echo $regex

rgclean $regex

echo "about to clean up dns records"
az network private-dns record-set list -g rg-cloudcore-network-dns-sbox -z eastus.az.mastercard.int | jq -r '.[] | select(.aRecords | length != 0) | .id' | xargs -I{} az resource delete --ids {}


