#!/bin/bash

loc=${BASH_SOURCE%/*};
if [[ -L ${loc}/tugboat ]]; then
    loc=$(dirname $($rl -f ${loc}/tugboat));
fi
printf '%s' $loc

for filename in ${loc}/*.sh; do
  [ -e "$filename" ] || continue;
  echo $filename
  fullpath=$(readlink -f $filename)
  name=$(basename $filename .sh);
  if [[ ! -L /usr/local/bin/$name ]]; then
    echo "making $name linked to $fullpath"
    sudo ln -s $fullpath /usr/local/bin/$name
  fi
done

alias dc='docker-compose'

source $loc/cdr.sh 
