#!/bin/bash

check=$(pwd);
makepath="/";

check_array=(${check//\// });

for (( i = ${#check_array[@]}; i != 0; i-- ))
do
  slice_array=("${check_array[@]:0:$i}")
  checkpath="$(printf "/%s" "${slice_array[@]}")"
  if [[ -f "$checkpath/Makefile" ]]; then
    echo "makefile found at $checkpath"
    makepath=$checkpath
    break
  fi
done

pushd $makepath
if [[ -f "Makefile" ]]; then
  /usr/bin/make "$@"
else
  echo "no makefile found!!"
fi
popd

