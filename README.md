# tugboat

An ephemeral development environment maker running under Docker.  **Emphasis on ephemeral.**

## installation

Provided you have Docker installed (tested with 17+), installation is pretty easy:

```bash
curl -sSLk https://fusion.mastercard.int/stash/users/e082491/repos/mc-tugboat/raw/install-tugboat.sh?at=refs%2Ftags%2Fv0.1.2 | bash --
```

Currently, this assumes a RHEL-based distro (Centos, RHEL and OEL).

## usage

`tugboat [subcommand] (name)`

### subcommands

* **`help`**: prints help.  (shock.)
* **`use (name)`**: builds the image, starts the container, and drops the user into a `bash` session on the container.
* **`scuttle (name)`**: destroys the container and image.
* **`nuke`**: destroys all of the environments.  All. Of. Them.  (You have been warned.)

## directory structure

By default, `tugboat` will assume the `tugboat.sh` is at the same directory as the folders containing the Dockerfiles.  For example:

```
- tugboat.sh
- my-cool-container
    - Dockerfile
- my-other-container
    - Dockerfile
    - run.sh
```

Running `tugboat use my-cool-container` in this case will build and run the Dockerfile in that directory.

## i want to use a regular ol' `Dockerfile`

`tugboat` will build and run the Dockerfile using the containing folder as the hostname, image name, and container name.  `ARG` directives without defaults in the Dockerfiles will attempt to use the host's environment variable of the same name.  Finally, `EXPOSE` directives will be exposed as-is on the port specifed (eg, `EXPOSE 8888/tcp` will result in an argument of the form `-p 8888:8888/tcp`).

## i'd rather `run` stuff myself

If custom functionality at start time is needed, `tugboat` will look for a `run.sh` script before building the Dockerfile.  If such a file exists, `tugboat` will assume `run.sh` runs a container from the image.  For example, if an image requires ports to be exposed other than what the `EXPOSE` directive dictates, use a `run.sh`.  (You'll have to mount `/src` yourself.  Take a look at `jumpbox`, for example.)

## some other miscellaneous notes about `use`

* Images can inherit from other images in the local folder structure.  Think of this like an _extremely_ discount version of a local Docker repository.
* Local folder structure trumps the Docker repository.
* If a file in the image directory for a given image (or files in an image it inherits from) has been touched since the image was created, `tugboat` will `scuttle` everything downstream of that image including the image.
* For now, all containers run with the default behavior will mount the local `$HOME/src` to `/src` under the container.

## improvements/todos

* **tests.** Implementing [`bats`](https://github.com/sstephenson/bats) tests would be pretty nice.
* **debian support.** Should be easy, use `update-alternatives` instead of `alternatives`.
* **custom build support.** In addition to custom `run.sh`, a custom `build.sh` could also be looked for to provide maximum flexibility.
