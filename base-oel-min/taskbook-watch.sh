#!/bin/bash
mkdir -p /src/.taskbook
cp -r /src/taskbook/archive /src/.taskbook
cp -r /src/taskbook/storage /src/.taskbook
cp /src/taskbook/taskbook.json ~/.taskbook.json

if ! pgrep -x "inotifywait"; then
  inotifywait \
    -r \
    -m \
    -e moved_to \
    -e moved_from \
    -e move \
    -e create \
    -e delete \
    /src/.taskbook | 
    while read -r filename event; do
      if echo $filename | grep -v '.git' | grep -v '.temp' 2>&1 >/dev/null; then
        pushd /src/taskbook > /dev/null
          cp -r /src/.taskbook/storage .
          cp -r /src/.taskbook/archive .
          git add --all 2>&1 1>/dev/null
          git commit --quiet -am "$event on $filename" 2>&1 >/dev/null
          git push --quiet
        popd > /dev/null
      fi
    done
fi
