## future bashhack

need to add git commitdate update to bashrc:

 ```
 LC_ALL=C GIT_COMMITTER_DATE="$(date)" git commit --amend --no-edit  --date "$(date)"
 ```

## super-sweet inotify

 run this on the host to up the listenercount:

 ```
 echo fs.inotify.max_user_watches=524288 | sudo tee -a /etc/sysctl.conf && sudo sysctl -p
 ```
 
