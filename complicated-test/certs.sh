#!/bin/bash
     echo "Q" |  openssl s_client -showcerts -connect globalrepository.mclocal.int:443 2>&1 | sed -ne '/-BEGIN CERTIFICATE-/,/-END CERTIFICATE-/p' > /etc/ssl/certs/globalrepo.crt && \
     echo "Q" |  openssl s_client -showcerts -connect github.com:443 2>&1 | sed -ne '/-BEGIN CERTIFICATE-/,/-END CERTIFICATE-/p' >> /etc/ssl/certs/github.crt && \
     echo "Q" |  openssl s_client -showcerts -connect go.googlesource.com:443 2>&1 | sed -ne '/-BEGIN CERTIFICATE-/,/-END CERTIFICATE-/p' >> /etc/ssl/certs/gosource.crt && \
     echo "Q" |  openssl s_client -showcerts -connect gitlab.howett.net:443 2>&1 | sed -ne '/-BEGIN CERTIFICATE-/,/-END CERTIFICATE-/p' >> /etc/ssl/certs/gitlab.howett.crt && \
     echo "Q" |  openssl s_client -showcerts -connect gopkg.in:443 2>&1 | sed -ne '/-BEGIN CERTIFICATE-/,/-END CERTIFICATE-/p' >> /etc/ssl/certs/gopkg.crt && \
     echo "Q" |  openssl s_client -showcerts -connect artifacts.mastercard.int:443 2>&1 | sed -ne '/-BEGIN CERTIFICATE-/,/-END CERTIFICATE-/p' >> /etc/ssl/certs/artifactory.crt && \
     echo "Q" |  openssl s_client -showcerts -connect releases.hashicorp.com:443 2>&1 | sed -ne '/-BEGIN CERTIFICATE-/,/-END CERTIFICATE-/p' >> /etc/ssl/certs/hashirelease.crt && \

     cat /etc/ssl/certs/artifactory.crt >> /etc/ssl/certs/ca-certificates.crt && \
     cat /etc/ssl/certs/hashirelease.crt >> /etc/ssl/certs/ca-certificates.crt && \
     cat /etc/ssl/certs/globalrepo.crt >> /etc/ssl/certs/ca-certificates.crt && \

     cat /etc/ssl/certs/artifactory.crt >> /etc/ssl/cert.pem && \
     cat /etc/ssl/certs/hashirelease.crt >> /etc/ssl/cert.pem && \
     cat /etc/ssl/certs/globalrepo.crt >> /etc/ssl/cert.pem
